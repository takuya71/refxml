package refxml

import org.eclipse.jetty.servlet.DefaultServlet
import org.eclipse.jetty.util.resource.Resource

/**
 * Created with IntelliJ IDEA.
 * User: takuya
 * Date: 2013/06/24
 * Time: 23:41
 * To change this template use File | Settings | File Templates.
 */
class MyDefaultServlet extends DefaultServlet {
  override def getResource(resouceString: String): Resource =
  {
    val url = this.getClass().getResource(resouceString)
    Resource.newResource(url)
  }
}
