package refxml

import org.scalatra._
import scala.util._

class refxml extends ScalatraServlet {

  get("/") {

    redirect("/refxml")
  }

  get("/refxml") {
    val xmlarea:String = "<a>sample</a>"
    html.refxml.render(xmlarea)
  }

  post("/refxml") {

    import scala.xml._
    val xmls = Try {
      XML.loadString(params("xmlarea"))
    }

    val formattedXml = xmls match {
      case Success(x) => new PrettyPrinter(80,2).format(x)
      case Failure(e) => "ERROR: " + e.toString
    }

    html.refxml.render(formattedXml)
  }

  get("/jsxml") {
    html.jsxml.render("sample")
  }
}
